//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by sinJect.rc
//
#define IDD_DLGMAIN                     101
#define IDI_ICON                        102
#define IDR_WAVE_INJECT                 104
#define IDS_APPTITLE                    105
#define IDS_LBL_HINT                    106
#define IDS_BTN_MANUALLOAD              107
#define IDS_GRP_MANUALLOAD              108
#define IDS_CHK_AUTOCLOSE               109
#define IDS_GRP_SELECTDLL               110
#define IDS_GRP_SELECTPROFILE           111
#define IDS_BTN_ADDPROFILE              112
#define IDS_BTN_DELETEPROFILE           113
#define IDS_DLG_TITLE                   114
#define IDS_TRAY_MONINTERING            115
#define IDS_MSG_INJECTTHREADFAILED      116
#define IDS_MSG_FILENOTFOUND            117
#define IDS_OFD_FILTER                  118
#define IDS_OFD_SELECTEXE               119
#define IDS_MSG_MANUALLOADFAILED        120
#define IDS_MSG_NOEXEORDLL              121
#define IDS_MSG_NEEDADMIN               122
#define IDS_MSG_NEEDXP                  123
#define IDS_MSG_GETVERSIONFAILED        124
#define IDC_LISTGAMES                   1001
#define IDC_LISTDLLS                    1002
#define IDC_BTN_MANUAL                  1003
#define IDC_BTN_ADDGAME                 1004
#define IDC_BTN_DELGAME                 1005
#define IDC_EDIT_CMDLINE                1006
#define IDC_CHKAUTOCLOSE                1018
#define IDC_CHK_AUTOCLOSE               1018
#define IDC_GRP_SELECTPROFILE           1019
#define IDC_GRP_SELECTDLL               1020
#define IDC_GRP_MANUALLOAD              1021
#define IDC_LBL_HINT                    1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
