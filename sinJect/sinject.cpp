#include "sinject.h"

// Global Variables
bool bInjected = false;
bool pauseLoop = false;

// Declarations
BOOL InjectIntoProcess(WCHAR *szExePath, WCHAR *szExeName, WCHAR szDllName[MAX_DLLS][MAX_PATH]);
BOOL CheckForProcess(WCHAR *szExeName);

DWORD InjectionTh()
{
	while(1)
	{
		if(!pauseLoop && InjectIntoProcess(szExePath, szExeName, szDllName))
		{
			if(bAutoClose && g_hWnd)
			{
				SendMessage(g_hWnd,WM_CLOSE,0,0);
				pauseLoop=true;
				return 0;
			}
			else
			{
				PlaySound(MAKEINTRESOURCE(IDR_WAVE_INJECT), GetModuleHandle(NULL), SND_RESOURCE|SND_ASYNC);
			}
			
			while(1)
			{
				if(!CheckForProcess(szExeName))
					break;

				Sleep(5000);
			}
		}

		Sleep(100);
	}

	return 0;
}

DWORD InjectionTh2()
{
	// Wait for the exe to init
	while(!CheckForProcess(szExeName))
	{
		Sleep(1000);
	}

	// Inject the rest of the DLLs
	InjectIntoProcess(szExePath, szExeName, szDllName);

	if(bAutoClose)
	{
		SendMessage(g_hWnd,WM_CLOSE,0,0);
		pauseLoop=true;
		return 0;
	}

	while(1)
	{
		if(!CheckForProcess(szExeName))
				break;

		Sleep(5000);
	}

	pauseLoop = false;
	return 0;
}

BOOL InjectStartProcess(WCHAR *szExePath, WCHAR szDllName[MAX_DLLS][MAX_PATH])
{
	WCHAR szDllNameAndPath[MAX_PATH];
	WCHAR szFilePath[MAX_PATH];

	ZeroMemory(szFilePath,sizeof(szFilePath));
	ZeroMemory(szDllNameAndPath,sizeof(szDllNameAndPath));

	pauseLoop = true;

	if(!wcslen(szExePath) || !wcslen(szDllName[0]))
	{
		WCHAR buffer[256];
		LoadString(NULL,IDS_MSG_NOEXEORDLL,buffer,256);
		MSGBOX(buffer);
		return FALSE;
	}
	// Get the full paths to the first DLL
	GetCurrentDirectory(MAX_PATH, szDllNameAndPath);
	wcscat_s(szDllNameAndPath, L"\\");
	wcscat_s(szDllNameAndPath, szDllName[0]);

	// Get exe path without exe name
	wcscpy_s(szFilePath, szExePath);
	size_t len = wcslen(szFilePath);
	
	while(len && szFilePath[len] != L'\\')
		len--;

	if(len)
		szFilePath[len + 1] = L'\0';

	// Create process with dll
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si,sizeof(si));
	ZeroMemory(&pi, sizeof(pi));
	si.cb = sizeof(si);
	
	WCHAR cmdLine[1024];
	ZeroMemory(cmdLine, sizeof(cmdLine));
	swprintf_s(cmdLine, L" %s\0", szCmdLine);

	if(DetourCreateProcessWithDll
		(szExePath, cmdLine, NULL, NULL, TRUE,
		CREATE_DEFAULT_ERROR_MODE, NULL, szFilePath,
		&si, &pi, szDllNameAndPath,NULL))
	{
		PlaySound(MAKEINTRESOURCE(IDR_WAVE_INJECT), GetModuleHandle(NULL), SND_RESOURCE|SND_ASYNC);
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)InjectionTh2, 0, 0, 0);
		return TRUE;
	}
	LoadString(NULL, IDS_MSG_MANUALLOADFAILED,cmdLine,1024);
	MSGBOX(cmdLine);
	pauseLoop = false;
	return FALSE;
}

BOOL InjectIntoProcess(WCHAR *szExePath, WCHAR *szExeName, WCHAR szDllName[MAX_DLLS][MAX_PATH])
{
	HANDLE hHandle = NULL;
	WCHAR szProcessName[MAX_PATH], szProcessName2[MAX_PATH];
	WCHAR szDllNameAndPath[MAX_DLLS][MAX_PATH];
    DWORD aProcesses[1024], cb, cProcesses;
	HANDLE hProcess = NULL;
	HMODULE hMod = NULL;
	UINT i = 0;

	ZeroMemory(szProcessName,sizeof(szProcessName));
	ZeroMemory(szProcessName2,sizeof(szProcessName2));
	ZeroMemory(szDllNameAndPath,sizeof(szDllNameAndPath));

	if(!wcslen(szExePath) || !wcslen(szExeName) || !wcslen(szDllName[0]))
		return FALSE;

	// Trying to open a handle like this will tell us whether ET is running or not
	hHandle = CreateFile(szExePath, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD err=GetLastError();
	if(hHandle == INVALID_HANDLE_VALUE && err == ERROR_SHARING_VIOLATION)
	{
		wcscpy_s(szProcessName2,MAX_PATH,szExeName);

		// Get the full paths to the DLLs for later use
		for(i=0; i<MAX_DLLS; i++)
		{
			if(!wcslen(szDllName[i]))
				continue;

			GetCurrentDirectory(MAX_PATH, szDllNameAndPath[i]);
			wcscat_s(szDllNameAndPath[i], L"\\");
			wcscat_s(szDllNameAndPath[i], szDllName[i]);
		}

		// Get the list of process identifiers
		if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cb))
			return FALSE;

		// Calculate how many process identifiers were returned
		cProcesses = cb / sizeof(DWORD);

		// Get the name and process identifier for each process
		for(i=0; i<cProcesses; i++)
		{
			hProcess = OpenProcess(2035711, FALSE, aProcesses[i]);

			if(hProcess)
			{
				if(EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cb))
				{
					GetModuleBaseName(hProcess, hMod, szProcessName, MAX_PATH);
				}
				else
				{
					ZeroMemory(szProcessName,sizeof(szProcessName));
				}
				
				// Convert both to lowercase
				_wcslwr_s(szProcessName);
				_wcslwr_s(szProcessName2);

				if(wcscmp(szProcessName, szProcessName2) == 0)
				{
					// We found the process, inject our DLLs
					for(int i=0; i<MAX_DLLS; i++)
					{
						if(!wcslen(szDllNameAndPath[i]))
							continue;

						DetourContinueProcessWithDll(hProcess, szDllNameAndPath[i]);
					}

					// Cleanup
					CloseHandle(hProcess);
					CloseHandle(hHandle);
					return TRUE;
				}
			}

			CloseHandle(hProcess);
		}
	}

	CloseHandle(hHandle);
	return FALSE;
}

BOOL CheckForProcess(WCHAR *szExeName)
{
	WCHAR szProcessName[MAX_PATH], szProcessName2[MAX_PATH];
    DWORD aProcesses[1024], cb, cProcesses;
	HANDLE hProcess = NULL;
	HMODULE hMod = NULL;
	UINT i = 0;

	ZeroMemory(szProcessName,sizeof(szProcessName));
	ZeroMemory(szProcessName2,sizeof(szProcessName2));

	wcscpy_s(szProcessName2,MAX_PATH,szExeName);

	// Get the list of process identifiers
    if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cb))
        return FALSE;

    // Calculate how many process identifiers were returned
    cProcesses = cb / sizeof(DWORD);

    // Get the name and process identifier for each process
    for(i = 0; i < cProcesses; i++)
	{
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, aProcesses[i]);

		if(hProcess)
		{
			if(EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cb))
			{
				GetModuleBaseName(hProcess, hMod, szProcessName, sizeof(szProcessName)/sizeof(TCHAR));
			}

			// Convert both to lowercase
			_wcslwr_s(szProcessName);
			_wcslwr_s(szProcessName2);
			
			if(wcscmp(szProcessName, szProcessName2) == 0)
			{
				// We found the process
				return TRUE;
			}
		}

		CloseHandle(hProcess);
	}

	return FALSE;
}