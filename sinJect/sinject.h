/*
	sinJect by Sinner, don't forget to credit me!
	Please leave the APP_NAME & APP_AUTHOR alone!
	enjoy
*/

#pragma once
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "psapi.lib")
#pragma comment(lib, "winmm.lib")

#define WINVER 0x0501

#include <windows.h>
#include <stdio.h>
#include "detours.h" // 1.5
#include <psapi.h>
#include "resource.h"

// Constants
#define WM_TRAY (WM_USER+1)
#define APP_NAME L"sinJect" // pls dont edit
#define APP_AUTHOR L"Sinner" // pls dont edit
#define KEY_CMDLINE L"CmdLine"
#define KEY_PROFILES L"Profiles"
#define KEY_AUTOCLOSE L"AutoClose"
#define MAX_GAMES 25
#define MAX_DLLS 25
#define MSGBOX(msg) MessageBox(GetForegroundWindow(), msg, APP_NAME, MB_OK | MB_ICONINFORMATION);
#define APP_VERSION __TEXT("2.1.2.0")

// Externs
extern DWORD InjectionTh();
extern WCHAR szExeName[MAX_PATH];
extern WCHAR szExePath[MAX_PATH];
extern WCHAR szDllName[MAX_DLLS][MAX_PATH];
extern WCHAR szCmdLine[1024];
extern BOOL bAutoClose;
extern HWND g_hWnd;

BOOL InjectStartProcess(WCHAR *szExePath, WCHAR szDllName[MAX_DLLS][MAX_PATH]);