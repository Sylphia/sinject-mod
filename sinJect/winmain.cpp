#include "sinject.h"

// Global Variables
HINSTANCE g_hInstance = NULL;
HWND g_hWnd = NULL;
HANDLE hInjectionTh = NULL;
NOTIFYICONDATA trayIcon;
WCHAR szExeName[MAX_PATH];
WCHAR szExePath[MAX_PATH];
WCHAR szDllName[MAX_DLLS][MAX_PATH];
WCHAR szCmdLine[1024];
WCHAR fileFilter[] = L"*.dll";

BOOL bAutoClose = false;

// Declarations
LRESULT CALLBACK DlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
void TrayIcon(WPARAM wParam, LPARAM lParam);
void SaveToRegistry(WCHAR *path, WCHAR *key, WCHAR *value);
BOOL ReadFromRegistry(WCHAR *path, WCHAR *key, WCHAR *buffer);
int ReadFromRegistryByIndex(WCHAR *path, int index, WCHAR *buffer);
void DeleteFromRegistry(WCHAR *path, WCHAR *key);
bool PromptForFile(WCHAR *retFile, WCHAR *filter, WCHAR *title);
bool FileExists(const WCHAR *filename);

WINBASEAPI BOOL WINAPI CheckTokenMembership(HANDLE,PSID,PBOOL);

BOOL IsUserBelogsToAdministrators()
{
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup; 
	BOOL b;

	b = AllocateAndInitializeSid(&NtAuthority,
		2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&AdministratorsGroup); 
	if (b) 
	{
		if (!CheckTokenMembership(NULL, AdministratorsGroup, &b)) 
		{
			b = FALSE;
		} 
		FreeSid(AdministratorsGroup); 
	}

	return(b);
}

BOOL SetPrivilege(LPCTSTR lpszPrivilegeName, BOOL bEnable)
{
	HANDLE              hToken;
	TOKEN_PRIVILEGES    tp;
	LUID                luid;
	BOOL                ret;

	if (!OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY | TOKEN_READ,
		&hToken))
		return FALSE;

	if (!LookupPrivilegeValue(NULL, lpszPrivilegeName, &luid))
		return FALSE;

	tp.PrivilegeCount           = 1;
	tp.Privileges[0].Luid       = luid;
	tp.Privileges[0].Attributes = bEnable ? SE_PRIVILEGE_ENABLED : 0;

	ret = AdjustTokenPrivileges(hToken, FALSE, &tp, 0, NULL, NULL);

	CloseHandle(hToken);

	return ret;
}

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	g_hInstance = hInstance;
	HANDLE hToken   = NULL;
	BOOL   bRet   = FALSE;
	WCHAR stringBuffer[256];
	
	ZeroMemory(szExeName, sizeof(szExeName));
	ZeroMemory(szDllName, sizeof(szDllName));
	ZeroMemory(szCmdLine, sizeof(szCmdLine));

	OSVERSIONINFO    osvi;
	TOKEN_ELEVATION  elevation;
	DWORD            infoLen;
	TCHAR            szPath[MAX_PATH];
	SHELLEXECUTEINFO sei = { sizeof(sei) };
	BOOL             Elevated;
	
	BOOL forceTolangEN_US=FALSE;
	if  (GetUserDefaultUILanguage() != MAKELANGID(LANG_CHINESE,SUBLANG_CHINESE_SIMPLIFIED) &&
		GetUserDefaultUILanguage() != MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US)) 
	{ 
		forceTolangEN_US=TRUE;
	}

	// Get Windows version
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	if (!GetVersionEx(&osvi))
	{
		MSGBOX(__TEXT("Fatal: GetVersionEx() Failed!"));
		return -1;
	}

	if (osvi.dwMajorVersion >= 6)
	{
		if(forceTolangEN_US)
			SetThreadUILanguage(MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US));  
	}else
	{
		if(forceTolangEN_US)
			SetThreadLocale(MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US), SORT_DEFAULT)); 
	}

	// XP required
	if (osvi.dwMajorVersion < 5)
	{
		LoadString(g_hInstance, IDS_MSG_NEEDXP,stringBuffer,256);
		MSGBOX(stringBuffer);
		return -1;
	}

	Elevated = FALSE;

	// XP
	if (osvi.dwMajorVersion == 5)
	{
		if (IsUserBelogsToAdministrators())
			Elevated = TRUE;
	}
	// Vista
	else if (osvi.dwMajorVersion >= 6)
	{
		OpenProcessToken(GetCurrentProcess(), TOKEN_READ, &hToken);
		GetTokenInformation(hToken, TokenElevation, &elevation, sizeof(elevation), &infoLen);
		CloseHandle(hToken);

		if (elevation.TokenIsElevated != 0)
			Elevated = TRUE;
	}

	if (!Elevated)
	{
		// Elevate the process.
		if (GetModuleFileName(NULL, szPath, ARRAYSIZE(szPath)))
		{
			// Launch itself as administrator.
			sei.lpVerb = TEXT("runas");
			sei.lpFile = szPath;
			sei.hwnd = NULL;
			sei.nShow = SW_NORMAL;

			if (!ShellExecuteEx(&sei))
			{
				LoadString(g_hInstance, IDS_MSG_NEEDADMIN,stringBuffer,256);
				MSGBOX(stringBuffer);
				return -1;
			}
		}
		return 0;
	}

	SetPrivilege(SE_DEBUG_NAME, TRUE);

	// Initialize tray icon
	trayIcon.cbSize = sizeof(NOTIFYICONDATA);
	trayIcon.uCallbackMessage = WM_TRAY;
	trayIcon.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	trayIcon.uID = 1;
	trayIcon.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_ICON));

	LoadString(g_hInstance, IDS_TRAY_MONINTERING,stringBuffer,256);
	swprintf_s(trayIcon.szTip, stringBuffer);

	// Read registry
	ReadFromRegistry(NULL, KEY_CMDLINE, szCmdLine);

	// Initialize dialog box 
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_DLGMAIN), NULL, reinterpret_cast<DLGPROC>(DlgProc));

	// Show it
	//ShowWindow(g_hWnd, SW_SHOW);
	//UpdateWindow(g_hWnd);
	SetPrivilege(SE_DEBUG_NAME, FALSE);
	return 0;
}

LRESULT CALLBACK DlgProc(HWND hWndDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{
	WCHAR stringBuffer[256];
	// On init
	if(msg == WM_INITDIALOG)
	{
		g_hWnd=hWndDlg;
		// Set captions
		
		LoadString(g_hInstance, IDS_GRP_SELECTPROFILE,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_GRP_SELECTPROFILE),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_BTN_ADDPROFILE,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_BTN_ADDGAME),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_BTN_DELETEPROFILE,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_BTN_DELGAME),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_GRP_SELECTDLL,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_GRP_SELECTDLL),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_CHK_AUTOCLOSE,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_CHK_AUTOCLOSE),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_GRP_MANUALLOAD,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_GRP_MANUALLOAD),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_BTN_MANUALLOAD,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_BTN_MANUAL),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		LoadString(g_hInstance, IDS_LBL_HINT,stringBuffer,256);
		SendMessage(GetDlgItem(hWndDlg, IDC_LBL_HINT),WM_SETTEXT , NULL, (LPARAM)stringBuffer);
		
		LoadString(g_hInstance, IDS_DLG_TITLE,stringBuffer,256);
		WCHAR titleBuffer[256];
		swprintf_s(titleBuffer,256,__TEXT("%s %s"),stringBuffer,APP_VERSION);
		SendMessage(hWndDlg,WM_SETTEXT , NULL, (LPARAM)titleBuffer);

		// Set icons
		SendMessage(hWndDlg, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_ICON)));
		SendMessage(hWndDlg, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_ICON)));
		


		// Fill the game list ctrl with the info from registry
		WCHAR buffer[MAX_PATH];
		ZeroMemory(buffer, sizeof(buffer));
		for(int i=0; i<MAX_GAMES; i++)
		{
			if(!ReadFromRegistryByIndex(KEY_PROFILES, i, buffer))
				break;

			// Add it to the list
			if(wcslen(buffer))
				SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_ADDSTRING, NULL, (LPARAM)buffer);
		}

		// Select the first item in the list box by default & init szDllName with the value
		SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_SETCURSEL, 0, NULL);
		SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_GETTEXT, 0, (LPARAM)szExeName);
		ReadFromRegistry(KEY_PROFILES, szExeName, szExePath);
		WCHAR buff[MAX_PATH];
		ZeroMemory(buff,sizeof(buff));
		ReadFromRegistry(NULL, KEY_AUTOCLOSE, buff);
		bAutoClose=ReadFromRegistry(NULL, KEY_AUTOCLOSE, buff)?(0==wcsncmp(L"1",buff,1)):true;

		SendMessage(GetDlgItem(hWndDlg, IDC_CHK_AUTOCLOSE), BM_SETCHECK, bAutoClose ? BST_CHECKED:BST_UNCHECKED, 0);
		// Fill the dll list ctrl with the dll's in this directory
		DlgDirList(hWndDlg, fileFilter, IDC_LISTDLLS, 0, DDL_READWRITE|DDL_READONLY);

		// Select the first item in the list box by default & init szDllName with the value
		SendMessage(GetDlgItem(hWndDlg, IDC_LISTDLLS), LB_SETSEL, TRUE, 0);
		SendMessage(GetDlgItem(hWndDlg, IDC_LISTDLLS), LB_GETTEXT, 0, (LPARAM)szDllName[0]);

		// Fill in cmd line
		SendMessage(GetDlgItem(hWndDlg, IDC_EDIT_CMDLINE), WM_SETTEXT, NULL, (LPARAM)szCmdLine);
		
		// Injection thread loop
		if((hInjectionTh = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)InjectionTh, 0, 0, 0)) == NULL)
		{
			LoadString(g_hInstance, IDS_MSG_INJECTTHREADFAILED,stringBuffer,256);
			MessageBox(GetForegroundWindow(), stringBuffer, APP_NAME, MB_OK|MB_ICONERROR);
			return FALSE;
		}

		return TRUE;
	}

	// On destory
	else if(msg == WM_DESTROY || msg == WM_CLOSE)
	{
		CloseHandle(hInjectionTh);
		hInjectionTh = NULL;
		Shell_NotifyIcon(NIM_DELETE, &trayIcon);
		EndDialog(hWndDlg, 0);
		return TRUE;
	}

	// On window system btns/menu
	else if(msg == WM_SYSCOMMAND)
	{
		if(wParam == SC_MINIMIZE)
		{
			trayIcon.hWnd = hWndDlg;
			Shell_NotifyIcon(NIM_ADD, &trayIcon); 
			ShowWindow(hWndDlg, SW_HIDE);
			return TRUE;
		}
	}

	// On user control msg's
	else if(msg == WM_COMMAND)
	{
		if(LOWORD(wParam) == IDC_LISTDLLS)
		{
			if(HIWORD(wParam) == LBN_SELCHANGE)
			{
				int nBuffer[MAX_DLLS];
				ZeroMemory(nBuffer, MAX_DLLS);
				ZeroMemory(szDllName, sizeof(szDllName));

				HWND hListBox = GetDlgItem(hWndDlg, IDC_LISTDLLS);
				LRESULT itemsInBuffer = SendMessage(hListBox, LB_GETSELITEMS, MAX_DLLS, (LPARAM)nBuffer);
			
				for(int i=0; i<(int)itemsInBuffer; i++)
				{
					SendMessage(hListBox, LB_GETTEXT, nBuffer[i], (LPARAM)szDllName[i]);
				}
			}
		}

		else if(LOWORD(wParam) == IDC_LISTGAMES)
		{
			if(HIWORD(wParam) == LBN_SELCHANGE)
			{
				LRESULT selectedItm = SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_GETCURSEL, NULL, NULL);
				SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_GETTEXT, selectedItm, (LPARAM)szExeName);
				ReadFromRegistry(KEY_PROFILES, szExeName, szExePath);
			}
		}

		else if(LOWORD(wParam) == IDC_BTN_ADDGAME)
		{
			WCHAR filePath[MAX_PATH], fileExe[MAX_PATH];
			ZeroMemory(filePath, sizeof(filePath));
			ZeroMemory(fileExe, sizeof(fileExe));

			// Get full path to exe
			LoadString(g_hInstance, IDS_OFD_FILTER,stringBuffer,256);
			WCHAR ofdfilter[256];
			ZeroMemory(ofdfilter,sizeof(ofdfilter));
			int len=swprintf_s(ofdfilter,256,L"%s",stringBuffer);
			WCHAR* token=ofdfilter+len+1;
			len=swprintf_s(token,256-len-1,L"%s",L"*.exe");
			*(token+len+1)='\0';
			LoadString(g_hInstance, IDS_OFD_SELECTEXE,stringBuffer,256);
			BOOL result=PromptForFile(filePath, ofdfilter, stringBuffer);
			if(!result) return FALSE;
			if(!FileExists(filePath)) {
				LoadString(g_hInstance, IDS_MSG_FILENOTFOUND,stringBuffer,256);
				MSGBOX(stringBuffer);
				return FALSE;
			}

			// Extract exe name
			WCHAR *ptr = filePath;
			while(*ptr) *ptr++;
			while(*ptr != L'\\') *ptr--; *ptr++;
			wcscpy_s(fileExe, ptr);

			// Save it to the registry
			SaveToRegistry(KEY_PROFILES, fileExe, filePath);
			
			// Add it to the list
			SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_ADDSTRING, NULL, (LPARAM)fileExe);
		}

		else if(LOWORD(wParam) == IDC_BTN_DELGAME)
		{
			WCHAR fileExe[MAX_PATH];

			LRESULT selectedItm = SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_GETCURSEL, NULL, NULL);

			SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_GETTEXT, selectedItm, (LPARAM)fileExe);
			DeleteFromRegistry(KEY_PROFILES, fileExe);
			SendMessage(GetDlgItem(hWndDlg, IDC_LISTGAMES), LB_DELETESTRING, selectedItm, NULL);
		}

		else if(LOWORD(wParam) == IDC_BTN_MANUAL)
		{
			InjectStartProcess(szExePath, szDllName);
		}

		else if(LOWORD(wParam) == IDC_EDIT_CMDLINE)
		{
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				SendMessage((HWND)lParam, WM_GETTEXT, sizeof(szCmdLine), (LPARAM)szCmdLine);
				SaveToRegistry(NULL, KEY_CMDLINE, szCmdLine);
			}
		}

		else if(LOWORD(wParam) == IDC_CHK_AUTOCLOSE)
		{
			LRESULT result=SendMessage((HWND)lParam, BM_GETCHECK ,0,0);
			bAutoClose= result==BST_CHECKED;
			SaveToRegistry(NULL, KEY_AUTOCLOSE, bAutoClose? L"1":L"0");
		}
	}

	// On tray event
	else if(msg == WM_TRAY)
	{
		TrayIcon((WPARAM)hWndDlg, lParam);
	}

	return FALSE;
}

void TrayIcon(WPARAM wParam, LPARAM lParam)
{
    if((UINT)lParam == WM_LBUTTONDOWN)
    {
        Shell_NotifyIcon(NIM_DELETE, &trayIcon);
        ShowWindow((HWND)wParam, SW_SHOW);
    }
}

void SaveToRegistry(WCHAR *path, WCHAR *key, WCHAR *value)
{
	HKEY hKey = NULL;
	DWORD dwDispo = 0;
	WCHAR buf[MAX_PATH];

	if(!value)
		return;

	// No error checking I know, this is non-essential anyways
	if(path)
		swprintf_s(buf, L"Software\\%s\\%s\\%s\0", APP_AUTHOR, APP_NAME, path);
	else
		swprintf_s(buf, L"Software\\%s\\%s\0", APP_AUTHOR, APP_NAME);

	RegCreateKeyEx(HKEY_CURRENT_USER, buf, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDispo);
	RegSetValueEx(hKey, key, 0, REG_SZ, (UCHAR*)value, (DWORD)(wcslen(value)*sizeof(WCHAR)+1));
	RegCloseKey(hKey);
}

BOOL ReadFromRegistry(WCHAR *path, WCHAR *key, WCHAR *buffer)
{
	HKEY hKey = NULL;
	DWORD strLen = MAX_PATH;
	WCHAR buf[MAX_PATH];
	BOOL ret=FALSE;
	if(path)
		swprintf_s(buf, L"Software\\%s\\%s\\%s\0", APP_AUTHOR, APP_NAME, path);
	else
		swprintf_s(buf, L"Software\\%s\\%s\0", APP_AUTHOR, APP_NAME);

	RegOpenKeyEx(HKEY_CURRENT_USER, buf, NULL, KEY_ALL_ACCESS, &hKey);
	ret=ERROR_SUCCESS==RegQueryValueEx(hKey, key, NULL, NULL, (UCHAR*)buffer, &strLen);
	RegCloseKey(hKey);
	return ret;
}

int ReadFromRegistryByIndex(WCHAR *path, int index, WCHAR *buffer)
{
	HKEY hKey = NULL;
	DWORD strLen = MAX_PATH;
	DWORD strLen2 = MAX_PATH;
	WCHAR buf[MAX_PATH];
	DWORD dwDispo = 0;
	
	if(path)
		swprintf_s(buf, L"Software\\%s\\%s\\%s\0", APP_AUTHOR, APP_NAME, path);
	else
		swprintf_s(buf, L"Software\\%s\\%s\0", APP_AUTHOR, APP_NAME);

	RegCreateKeyEx(HKEY_CURRENT_USER, buf, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDispo);

	if(RegEnumValue(hKey, index, buffer, &strLen, NULL, NULL, (UCHAR*)buf, &strLen2) == ERROR_NO_MORE_ITEMS)
		return FALSE;

	RegCloseKey(hKey);
	return TRUE;
}

void DeleteFromRegistry(WCHAR *path, WCHAR *key)
{
	WCHAR buf[MAX_PATH];
	HKEY hKey = NULL;

	swprintf_s(buf, L"Software\\%s\\%s\\%s\0", APP_AUTHOR, APP_NAME, path);
	RegOpenKeyEx(HKEY_CURRENT_USER, buf, NULL, KEY_ALL_ACCESS, &hKey);
	RegDeleteValue(hKey, key);
	RegCloseKey(hKey);
}

bool PromptForFile(WCHAR *retFile, WCHAR *filter, WCHAR *title)
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hInstance = g_hInstance;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = retFile;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrFilter = filter;
	ofn.lpstrTitle  = title;
	ofn.Flags = OFN_FILEMUSTEXIST;

	return (GetOpenFileName(&ofn)!=0);
}

bool FileExists(const WCHAR *filename)
{
	WIN32_FIND_DATA finddata;
	HANDLE handle = FindFirstFile(filename,&finddata);
	return (handle!=INVALID_HANDLE_VALUE);
}
